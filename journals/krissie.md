## 27FEB2024 EOD Report

Today, we setup our database that we will be using in the docker-compose.yaml file. We decided to
continue with postgresSQL. We added all our journals and the api/queries/accounts.py.

## 28FEB2024 EOD Report

Group is feeling they need more time to study so we are taking time with the modules.
I have started to type the backend auth code in our VSCode to get started due to time
constraints. We reviewed as a group what was typed in order for all of us to be
on the same page. I've created the api/authenticator.py, api/models.py, api/main.py,
and api/routers/accounts.py

## March 1, 2024

Today group worked on Backend Auth. We could not work through the errors, so we decided it would be best to take time to reflect and review, such as watching lectures, videos online, reading documents and reviewing learn.

## March 4, 2024

Today group worked on **Frontend Auth**. We decided it would be best to review documents, lectures, and videos about FastAPI routes. We decided that tomorrow, March, 5, we would start coding.
