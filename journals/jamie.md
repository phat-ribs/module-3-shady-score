In the journals, every day that you work on the project, you must make an entry in your journal after you've finished that day. At a minimum, you'll need to include the following information in each entry:

The date of the entry
A list of features/issues that you worked on and who you worked with, if applicable
A reflection on any design conversations that you had
At least one ah-ha! moment that you had during your coding, however small
Keep your journal in reverse chronological order. Always put new entries at the top.

Journal Entry #1 2/27/2024
Today, we each set up our journals and we all pushed.
We also chose and setup the postgresql database, we
followed the learn card and made the adjustments to
our code.

Journal Entry #2 2/28/2024
yml, queries, authenticator

Journal Entry #3 2/29/2024
Auth

Journal Entry #4 3/1/2024
we got stuck on auth today with no success

Journal Entry #5 3/4/2024
we restarted our code and all worked together to get backend auth to work

Journal Entry #6 3/5/2024
wrote the skeletons to our routes. spent most of our day discussing the specifications of our diagram

Journal Entry #7 3/6/2024
driver - brittany
migrations postgres tables. models, and routes links.

Journal Entry #8 3/7/2024
driver - krissie
worked on routers, docs,

Journal Entry #9 3/8/2024
driver - jamie
Collaborating on backend routes with my teammates.

Journal Entry #10 3/11/2024
driver - jamie
Made good progress understanding FastAPI routes. Working on basic CRUD operations.

Journal Entry #11 3/12/2024
driver - jamie
I believe we didnt make a good structure on our excalidraw and we're behind because of it. trying to remake some ideas and possibly shrink down the project to make it more manageable

Journal Entry #12 3/13/2024
driver - brittany

Journal Entry #13 3/14/2024
driver - brittany

Journal Entry #14 3/15/2024
driver - brittany
Got stuck on a routing issue. My endpoint isn't connecting properly to the database. Asked for help from my team, but no luck yet.

Journal Entry #15 3/18/2024
driver - krissie
Spent the past couple of days debugging with my team. Found out I was missing a parameter in my route definition. Feeling relieved we figured it out together!

Journal Entry #16 3/19/2024
driver - krissie
Working on authentication routes now. It's a bit more complex than I anticipated. Grateful for my team's support as we navigate this together.

Journal Entry #17 3/20/2024
driver - krissie
Made progress on authentication routes, but ran into another issue with token generation. Teammate suggested a different approach, so we're testing it out.

Journal Entry #18 3/21/2024
driver - jamie
Fixed the token generation issue! Turns out, I was missing a key configuration step

Journal Entry #19 3/22/2024
driver - jamie
Back to working on CRUD routes. I keep forgetting to add the correct HTTP methods to my route definitions. My team has been great at catching these mistakes.

Finally completed the CRUD routes! It was a long journey, but I've learned so much. Grateful for my team's support and looking forward to the next challenge.
