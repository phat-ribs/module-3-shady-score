<!-- In the journals, every day that you work on the project, you must make an entry in your journal after you've finished that day. At a minimum, you'll need to include the following information in each entry:

The date of the entry
A list of features/issues that you worked on and who you worked with, if applicable
A reflection on any design conversations that you had
At least one ah-ha! moment that you had during your coding, however small
Keep your journal in reverse chronological order. Always put new entries at the top. -->

# March 23, 2024

- LoginForm

Today I worked on,

I fixed the login form, and now we are working on fixing the shelves page so that it will populate onto the webpage. We were missing an email input for our login form. 

# March 22, 2024 (Last day extended to March 25)

- unit test

Today I worked on,

I am still trying to figure out how to get my unit tests to function properly.

## March 21, 2024

-   Backend auth, unit test

Today we worked on:

Today I worked on unit tests and backend auth.

## March 19, 2024

-   Frontend auth, CSS, Protecting Routes, Model Changes

Today we worked on:

Today I worked on more CSS, such as changing the way the buttons look and adding a logo with animation. We found out what the problem is for our front end auth and fixed it by changing the backend. Some game models had to be added along with other models. Now, the signup and login forms work, along with Swagger and pgAdmin. Our SEIR said we need to focus on protecting the routes so that people can only interact with our webpage if they are signed up and logged in.

## March 18, 2024

-   CSS, Frontend Auth, login form, signup form, landing page

Today we worked on:

Today I worked on the CSS for the landing page, login form and signup form. The front end auth still needs work because the signup and login forms are not going through, and now the fast API is down in Docker. On the other end, we are also editing our backend and rewriting some of that code to get the fastApi and login and signup forms working.

## March 15, 2024

Day off.

## March 14, 2024

-   Webpage populating, Error "failed to get token"

We continued working on the front end and finally got things to populate onto the webpage. There is an error that is stating "Failed to get token", so the signup and login functions are not working currently.

## March 13, 2024

-   Creating front end, auth.jsx, Home.jsx, Landingpage.jsx etc.

Today I worked on:

Today I was the driver for the front end portion of the group. We worked on creating a components folder and inside I created auth.jsx, Home.jsx, LandingPage.jsx, Search.jsx, Titlebar.jsx, and TokenCard.jsx. I also created the App.jsx, Index.jsx, Main.jsx, and Nav.jsx.

## March 12, 2024

-   FINISHED BACKEND AUTH

We finished backend auth with the exception of a couple errors.

## March 11, 2024

We worked on finishing up backend auth.

## March 8, 2024

Not much happened today because of social hack hour.

## March 5, 2024

Today, I worked on:

Today we focused on backend auth.

## March 4, 2024

Today, I worked on:

-   Frontend Auth

Today my group worked on **Frontend Auth**. We could not figure out exactly what our next step in our process would be, so after discussion and review, we decided it would be best to review documents, lectures, and videos about FastAPI routes. We decided that tomorrow, March, 5, we would start coding.

## March 1, 2024

-   Backend Auth

Today, I worked on:

Today my group worked on Backend Auth. We could not figure out our issue today, so we decided it would be best to take time to reflect and review, such as watching lectures, videos online, reading documents and reviewing learn.

## February 28, 2024

Today, I worked on:

-   Paths, FastAPI, Login, logout, Token,

We worked on creating the back end auth for our app, creating the paths, learning the ins and outs of FastAPI such as the login token, logout token and create a user. I learned a little more about third party API's through a web document and I now understand third party API's a little more.

## February 28, 2024

Today, I worked on:

-   Catching up

The group focused on strengthening our understanding of backend auth through the Learn modules and caught up on some of the other learn modules. We also took time to review the recorded lectures.

## February 27, 2024

Today, I worked on:

-   Cloning the git repo for module-3-shady-score, creating volumes, building my docker containers, setting up environment variables, setting up our database and setting up my pgAdmin database.

Everyone in my group worked together to start the beginning of our app, ShadyScore. We cloned the repo from GitLab and began setting up the basics of our app. We began creating our environment variables which will change when our app deploys, along with our FastAPI, React and secret variables. We then set up our PostgreSQL database, and after making sure everything is good to go, we created an account for pgAdmin and created a database in there for our ShadyScore app.

We named our directory module-3-shady-score and the name of our group is PhatRibs.
