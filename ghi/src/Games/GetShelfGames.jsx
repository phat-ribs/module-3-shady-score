// import React, { useState, useEffect } from 'react'

// function UsersShelvesWithGames() {
//     const [usersData, setUsersData] = useState([])

//     useEffect(() => {
//         const fetchData = async () => {
//             const response = await fetch(
//                 'http://localhost:8000/api/users/shelves/games'
//             )
//             if (response.ok) {
//                 const data = await response.json()
//                 setUsersData(data)
//             } else {
//                 console.error('An error occurred fetching the data')
//             }
//         }
//         fetchData()
//     }, [])

//     return (
//         <div>
//             {usersData.map((user) => (
//                 <div key={user.id}>
//                     <h3>{user.username}</h3>
//                     {user.shelves.map((shelf) => (
//                         <div key={shelf.id}>
//                             <h4>{shelf.name}</h4>
//                             <ul>
//                                 {shelf.games.map((game) => (
//                                     <li key={game.id}>{game.name}</li>
//                                 ))}
//                             </ul>
//                         </div>
//                     ))}
//                 </div>
//             ))}
//         </div>
//     )
// }

// export default UsersShelvesWithGames

import React, { useState, useEffect } from 'react'

function UserGames() {
    const [games, setGames] = useState([])
    const [userId, setUserId] = useState('')

    const handleChange = (e) => {
        setUserId(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const response = await fetch(
            `http://localhost:5000/api/games?user_id=${userId}`
        )
        if (response.ok) {
            const data = await response.json()
            setGames(data.games)
        } else {
            console.error('An error occurred fetching the data')
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={userId} onChange={handleChange} />
                <button type="submit">Get Games</button>
            </form>
            <ul>
                {games.map((game) => (
                    <li key={game.id}>{game.name}</li>
                ))}
            </ul>
        </div>
    )
}

export default UserGames
