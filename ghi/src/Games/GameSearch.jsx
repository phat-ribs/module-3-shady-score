import React, { useState } from 'react'

function SearchComponent() {
    const [query, setQuery] = useState('')
    const [searchResults, setSearchResults] = useState([])
    const [selectedGame, setSelectedGame] = useState(null)

    const handleChange = (e) => {
        setQuery(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const response = await fetch(
            `https://api.rawg.io/api/games?key=${'api_key'}`
        )
        if (response.ok) {
            const data = await response.json()
            setSearchResults(data.results)
            setSelectedGame(null) 
        } else {
            console.error('An error occurred fetching the data')
        }
    }

    const handleGameClick = async (gameId) => {
        const response = await fetch(`https://api.rawg.io/api/games/${gameId}`)
        if (response.ok) {
            const game = await response.json()
            setSelectedGame(game)
        } else {
            console.error('An error occurred fetching the game details')
        }
    }

    const handleAddToShelf = async () => {
        if (selectedGame) {
            const response = await fetch('/add_game_to_shelf', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(selectedGame),
            })
            if (response.ok) {
                console.log('Game added to shelf successfully')
            } else {
                console.error('Failed to add game to shelf')
            }
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input type="text" value={query} onChange={handleChange} />
                <button type="submit">Search</button>
            </form>
            <ul>
                {searchResults.map((result) => (
                    <li
                        key={result.id}
                        onClick={() => handleGameClick(result.id)}
                    >
                        {result.name}
                    </li>
                ))}
            </ul>
            {selectedGame && (
                <div>
                    <h2>{selectedGame.name}</h2>
                    <p>{selectedGame.description}</p>
                    {}
                    <button onClick={handleAddToShelf}>Add to Shelf</button>
                </div>
            )}
        </div>
    )
}

export default SearchComponent
