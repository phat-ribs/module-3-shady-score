import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const GameDetails = () => {
    const { gameId } = useParams()
    const [gameDetails, setGameDetails] = useState(null)

    useEffect(() => {
        const fetchGameDetails = async () => {
            const response = await fetch(`${YOUR_BACKEND_URL}/games/${gameId}`)
            if (response.ok) {
                const data = await response.json()
                setGameDetails(data)
            }
        }

        fetchGameDetails()
    }, [gameId])

    const addToShelf = async (shelfId) => {
        const response = await fetch(
            `${YOUR_BACKEND_URL}/shelves/${shelfId}/games/`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer {USER_TOKEN}`,
                },
                body: JSON.stringify({ gameId }),
            }
        )

        if (response.ok) {

        }
    }

    return (
        <div>
            {gameDetails ? (
                <>
                    <h2>{gameDetails.name}</h2>
                    <p>{gameDetails.description}</p>
                    <button onClick={() => addToShelf(YOUR_SHELF_ID)}>
                        Add to Shelf
                    </button>
                </>
            ) : (
                <p>Loading...</p>
            )}
        </div>
    )
}

export default GameDetails
