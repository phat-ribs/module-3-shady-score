import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './Store/store'
import App from './app/App'

const rootElement = document.getElementById('root')

const domain = /https:\/\/[^/]+/
const basename = process.env.PUBLIC_URL.replace(domain, '')

ReactDOM.createRoot(rootElement).render(
    <React.StrictMode>
        <Router basename={basename}>
            <Provider store={store}>
                <App />
            </Provider>
        </Router>
        <ToastContainer />
    </React.StrictMode>
);
