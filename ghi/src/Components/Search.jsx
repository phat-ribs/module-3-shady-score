import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

const SearchResults = () => {
    const navigate = useNavigate()
    const location = useLocation()
    const [searchGames, setSearchGames] = useState([])
    const query = location.search?.query // Assuming you pass the search query in location state

    useEffect(() => {
        // Function to fetch search results from your backend
        const fetchSearchResults = async () => {
            const response = await fetch(
                `{YOUR_BACKEND_URL}/search/games/?query=${query}`
            )
            if (response.ok) {
                const data = await response.json()
                setSearchGames(data)
            }
        }

        if (query) {
            fetchSearchResults()
        }
    }, [query])

    return (
        <div>
            <h2>Search Results for "{query}"</h2>
            {searchGames.length > 0 ? (
                <ul>
                    {searchGames.map((game) => (
                        <li key={game.id}>
                            {game.name} -{' '}
                            <button
                                onClick={() => navigate(`/games/${game.id}`)}
                            >
                                View Details
                            </button>
                        </li>
                    ))}
                </ul>
            ) : (
                <p>No games found.</p>
            )}
        </div>
    )
}

export default SearchResults
