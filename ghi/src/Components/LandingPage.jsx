import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { Link } from 'react-router-dom'
import '../Styles.css/LandingPage.css'
import logo from '../Images/logo.png'

export default function LandingPage() {
    const { token } = useAuthContext()

    return (
        <>
            <div className="App">
                <img src={logo} alt="Logo Image" />
                <h1 className="display-5 fw-bold"></h1>
                <div className="col-lg-6 mx-auto"></div>
                <div className="button-container">
                    {!token && (
                        <Link
                            to="/login"
                            role="button"
                            className="original-button"
                            id="loginBtn"
                            type="submit"
                        >
                            LOG-IN!
                        </Link>
                    )}
                    {!token && (
                        <Link
                            to="/signUp"
                            role="button"
                            className="original-button"
                            id="signupBtn"
                            type="submit"
                        >
                            SIGN-UP!
                        </Link>
                    )}
                </div>
            </div>
        </>
    )
}
