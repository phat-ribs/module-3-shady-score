import LoginForm from '../Accounts/LoginForm'
import 'bootstrap/dist/js/bootstrap.bundle'
import 'react-json-pretty/themes/monikai.css'
import TokenCard from './TokenCard'
import useToken from '@galvanize-inc/jwtdown-for-react'

const ConsoleBanner = () => {
    return (
        <div className="alert alert-info mt-3 mb-3" role="alert">
            <i className="bi bi-info-circle-fill">
                Open your browser's console to see more information.
            </i>
        </div>
    )
}

export const Home = () => {
    const { token } = useToken()
    return (
        <div>
            <ConsoleBanner />
            {!token && <LoginForm />}
            {token && <TokenCard />}
        </div>
    )
}

export { ConsoleBanner }
