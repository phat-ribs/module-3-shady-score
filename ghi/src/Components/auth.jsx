export async function getToken(username, password) {
    const baseUrl = 'http://localhost:8000' 
    const url = `${baseUrl}/token`
    const form = new FormData()
    form.append('username', username)
    form.append('password', password)

    try {
        const response = await fetch(url, {
            method: 'POST',
            body: form,
        })

        if (!response.ok) {
            throw new Error('Failed to fetch token')
        }

        const data = await response.json()
        return data.access_token
    } catch (error) {
        throw new Error(`Failed to fetch token: ${error.message}`)
    }
}

export async function login(username, password) {
    try {
        const token = await getToken(username, password)
        return token
    } catch (error) {
        throw new Error(`Failed to get token after login: ${error.message}`)
    }
}

export async function register(accountData) {
    const baseUrl = import.meta.env.VITE_USER_SERVICE_API_HOST
    if (!baseUrl) {
        throw new Error('REACT_APP_USER_SERVICE_API_HOST is not set')
    }

    try {
        const response = await fetch(`${baseUrl}/api/accounts`, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(accountData),
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (!response.ok) {
            throw new Error(
                "Couldn't create account, please try a new username or email address"
            )
        }
    } catch (error) {
        throw new Error(`Failed to register account: ${error.message}`)
    }
}

export default { login, register, getToken }
