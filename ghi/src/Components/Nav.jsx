import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { Link } from 'react-router-dom'
import logo from '../Images/logo.png'
import '../Styles.css/Nav.css'

function Nav() {
    const { token } = useAuthContext()
    // const { logout } = useToken();

    const handleLogOut = async () => {
        const logOutUrl = `http://localhost:8000/token`

        const fetchConfig = {
            method: 'delete',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(logOutUrl, fetchConfig)
        if (response.ok) {
            navigate('/')
            window.location.reload()
        }
        if (!response.ok) {
            throw new Error('Failed to log out')
        }
    }

    return (
        <nav>
            <ul
                className="nav-links"
                style={{ display: 'flex', alignItems: 'center' }}
            >
                <li>
                    <Link to="/">
                        <img
                            src={logo}
                            alt="Logo"
                            className="logo"
                            style={{
                                width: '220px',
                                height: '55px',
                                borderRadius: '9px',
                            }}
                        />
                    </Link>
                </li>
                {!token && (
                    <>
                        <li>
                            <Link to="/signup">Signup</Link>
                        </li>
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                    </>
                )}
                {token && (
                    <>
                        <li>
                            <Link to="/shelves">My Shelves</Link>
                        </li>
                        <li>
                            <Link to="/search">Search</Link>
                        </li>
                        <li style={{ marginLeft: 'auto' }}>
                            <button
                                onClick={() => {
                                    handleLogOut()
                                }}
                                style={{ marginRight: '30px' }}
                            >
                                Logout
                            </button>
                        </li>
                    </>
                )}
            </ul>
        </nav>
    )
}

export default Nav
