import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const Shelf = () => {
    const { shelfId } = useParams()
    const [shelf, setShelf] = useState(null)

    useEffect(() => {
        const fetchShelf = async () => {
            const response = await fetch(`/shelves/${shelfId}/`)
            const data = await response.json()
            setShelf(data)
        }
        fetchShelf()
    }, [shelfId])

    return (
        <div>
            {shelf ? (
                <>
                    <h2>{shelf.name}</h2>
                    <ul>
                        {shelf.games.map((game) => (
                            <li key={game.id}>{game.name}</li>
                        ))}
                    </ul>
                </>
            ) : (
                <p>Loading shelf...</p>
            )}
        </div>
    )
}

export default Shelf;
