import { useEffect, useState } from 'react'

function Shelves() {
    const [shelves, setShelves] = useState([])

    const getData = async () => {
        const response = await fetch(`http://localhost:8000/api/shelves`);
        if (response.ok) {
            const data = await response.json()
            setShelves(data.shelves);
        } else {
            console.error('An error ocurred fetching the data')
        }
    }
    const handleDelete = async (shelf_id) => {
        const deleteUrl = `http://localhost:8000/api/shelves/${shelf_id}`
        await fetch(deleteUrl, {method: 'delete'})
        getData()
    }
    useEffect(() => {
        getData()
    }, []);


    return (
        <div>
            <h2>Shelves</h2>
            <ul>
                {shelves.map(shelf => {
                    return (
                    <li key={shelf.id}>{shelf.name}
                        <button
                            className="btn btn-danger"
                            onClick={() => handleDelete(shelf.id)}>
                            Delete
                        </button>
                    </li>
                    );
                })}
            </ul>
        </div>
    )
}

export default Shelves;
