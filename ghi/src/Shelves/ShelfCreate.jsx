import { useState } from 'react'
// import {Button} from 'react-bootstrap'
// import {Modal} from 'react-bootstrap'



function CreateShelfForm() {
    const [name, setName] = useState('')
    const [show, setShow] = useState(false)


    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = { name }

        const url = 'http://localhost:8000/api/shelves/shelfCreate/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        try {
            const response = await fetch(url, fetchOptions)
            if (response.ok) {
                const newShelf = await response.json()
                setName('')
                handleClose()
            } else {
                console.error('Submission failed', await response.text())
            }
        } catch (error) {
            console.error('An error occurred', error)
        }
    }

    const handleChangeName = (event) => {
        setName(event.target.value)
    }

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                Create Shelf
            </Button>{' '}
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Create a Shelf</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form onSubmit={handleSubmit} id="create-a-shelf-form">
                        <div className="col">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={handleChangeName}
                                    value={name}
                                    required
                                    placeholder="Name"
                                    type="text"
                                    id="name"
                                    name="name"
                                    className="form-control"
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="primary"
                        type="submit"
                        form="create-a-shelf-form"
                    >
                        Create
                    </Button>{' '}
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default CreateShelfForm
