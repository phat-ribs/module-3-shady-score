steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE games (
            id SERIAL PRIMARY KEY,
            name TEXT NOT NULL,
            description TEXT,
            platform VARCHAR(255),
            account_id INTEGER,
            shelf_id INTEGER,
            FOREIGN KEY (account_id) REFERENCES accounts(id) ON DELETE CASCADE,
            FOREIGN KEY (shelf_id) REFERENCES shelves(id) ON DELETE SET NULL
        );
        """,
        """
        DROP TABLE games;
        """
    ]
]
