steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE shelves (
            id SERIAL PRIMARY KEY,
            name VARCHAR(255),
            account_id INT NOT NULL,
            FOREIGN KEY(account_id) REFERENCES accounts(id) ON DELETE CASCADE
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE shelves;
        """
    ]
]
