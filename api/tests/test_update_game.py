from fastapi.testclient import TestClient
from main import app
from ..models import GameOut, GameUpdate

client = TestClient(app)

def test_update_game():

    game_id = 1
    data = GameUpdate(name="Updated Game", description="Updated Description", platform="Updated Platform")


    response = client.put(f"/games/{game_id}", json=data.dict())


    assert response.status_code == 200
    updated_game = GameOut(**response.json())
    assert updated_game.id == game_id
    assert updated_game.name == "Updated Game"
    assert updated_game.description == "Updated Description"
    assert updated_game.platform == "Updated Platform"