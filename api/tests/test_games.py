from fastapi.testclient import TestClient
from main import app
from queries.games import GamesQueries

client = TestClient(app=app)

class FakeGamesQueries:
    def get_game(self, game_id: int, account_id):
        return [
            {
                'id': 1, 'name': 'Skyrim', 'slug': 'test', 'released': 'test',
                'rating': 10, 'metacritic': 5, 'description': 'test',
                'background_image': 'test', 'shelf_id': 1
            }
        ]

    def test_get_game():
        app.dependency_overrides[GamesQueries] = FakeGamesQueries
        res = client.get('/api/games/100')
        data = res.json()

        assert res.status_code == 200
        assert data == {
            "id": 1,
            "name": "Skyrim",
            "slug": "test",
            "released": "test",
            "rating": 10,
            "metacritic": 5,
            "description": "test",
            "background_image": "test",
            "shelf_id": 1
        }

    def test_get_all_games():
        app.dependency_overrides[GamesQueries.get_all_games] = FakeGamesQueries.get_all_games
        res = client.get('/api/games')
        assert res.status_code == 200
        assert res.json() == {"games": [
            {
                'id': 1, 'name': 'Skyrim', 'slug': 'test', 'released': 'test',
                'rating': 10, 'metacritic': 5, 'description': 'test',
                'background_image': 'test', 'shelf_id': 1
            }
        ]}
