from fastapi.testclient import TestClient
from main import app
from queries.shelves import ShelvesQueries, ShelfOut


client = TestClient(app)


class MockShelfQueries:

    def get_user_shelf(self, shelf_id: int, account_id: int):
        return ShelfOut(
            shelf_id=shelf_id,
            name="shelf name 1",
            account_id=account_id
        )




def test_get_user_shelf():


    app.dependency_overrides[ShelvesQueries] = MockShelfQueries
    shelf_id = 1
    account_id = 1



    response = client.get(f"/api/shelves/accounts/{account_id}/{shelf_id}")



    assert response.status_code == 200

    shelf = response.json()
    assert len(shelf) == 3

    assert shelf == {
        "id":shelf_id,
        "name":"shelf name 1",
        "account_id":account_id
    }

    app.dependency_overrides={}
    