from fastapi import APIRouter, Depends, HTTPException
from queries.shelves import ShelvesQueries
from models import ShelfIn, ShelfOut, ShelfUpdate
from authenticator import authenticator
from typing import List


router = APIRouter(prefix="/api")


@router.post("/shelf/create")
async def create_shelf(
    shelf: ShelfIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    shelf_dict = shelf.dict()

    shelf_dict["account_id"] = account_id
    created_shelf = queries.create_shelf(shelf_dict)
    return created_shelf


@router.get("/shelf/games")
async def one_shelf_with_games(
    shelf_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    return queries.one_shelf_with_games(shelf_id, account_id)


@router.get("/shelves/games")
async def all_shelf_with_games(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    return queries.all_shelf_with_games(account_id)


@router.get("/shelves", response_model=List[ShelfOut])
async def get_user_shelves(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    return queries.get_user_shelves(account_id)


@router.get("/shelf", response_model=ShelfOut)
async def get_user_shelf(
    shelf_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    return queries.get_user_shelf(shelf_id, account_id)


@router.put("/shelf/{shelf_id}")
async def update_shelf(
    shelf_id: int,
    shelf: ShelfUpdate,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    account_id = account_data["id"]
    shelf_dict = shelf.dict()

    shelf_dict["account_id"] = account_id
    updated_shelf = queries.update_shelf(shelf_id, shelf_dict, account_id)
    if not updated_shelf:
        raise HTTPException(status_code=404, detail="Shelf not found")
    return updated_shelf



@router.delete("/shelf/{shelf_id}")
async def delete_shelf(
    shelf_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ShelvesQueries = Depends()
    ):
    success = queries.delete_shelf(shelf_id, account_data["id"])
    if success:
        return HTTPException(status_code=404, detail="Shelf deleted successfully")
    return {"message": "Shelf deleted successfully"}
