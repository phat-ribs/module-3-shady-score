from fastapi import APIRouter, Depends
from queries.games import GamesQueries
from models import GameUpdate, GameIn, GameOut
from authenticator import authenticator
from typing import List


router = APIRouter(prefix="/api")


@router.post("/games/create", response_model=GameOut)
async def create_game(
    game: GameIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: GamesQueries = Depends()
    ):
    account_id = account_data["id"]
    game_dict = game.dict()

    shelf_id = game_dict["shelf_id"]

    game_dict = game.dict()
    game_dict["shelf_id"] = shelf_id 
    game_dict["account_id"] = account_id
    created_game = queries.create_game(game_dict)
    return created_game


@router.get("/games/{game_id}")
async def get_game(
    game_id: int,
    shelf_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: GamesQueries = Depends()
    ):
    account_id = account_data['id']
    return queries.get_game(game_id, account_id, shelf_id)


@router.get("/games/shelves/{shelf_id}")
async def get_shelf_games(
    shelf_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: GamesQueries = Depends()
    ) -> List[GameOut]:
    account_id = account_data['id']
    return queries.get_shelf_games(account_id, shelf_id)


@router.get("/games", response_model=List[GameOut])
async def get_user_games(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: GamesQueries = Depends()
):
    account_id = account_data["id"]
    return queries.get_user_games(account_id)


@router.put("/games/{game_id}", response_model = GameOut)
def update_game(
    game_id: int,
    shelf_id: int,
    game: GameUpdate,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: GamesQueries = Depends(),
):
    account_id = account_data["id"]
    game_details = queries.get_game(game_id, account_id, shelf_id).dict()
    game_dict = game.dict()

    game_dict["shelf_id"] = game_details["shelf_id"]
    game_dict["account_id"] = account_id

    updated_game = queries.update_game(game_id, game_dict, account_id, shelf_id)
    return updated_game


@router.delete("/games/{game_id}", response_model=bool)
async def delete_game(
    game_id: int,
    queries: GamesQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return queries.delete_game(game_id, account_id)
