from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import List, Optional


class Account(BaseModel):
    username: str
    email: str


class AccountIn(Account):
    password: str


class AccountOut(Account):
    id: int


class AccountOutWithHashedPassword(Account):
    id: int
    hashed_password: str


class AccountToken(Token):
    account: AccountOut


class AccountForm(BaseModel):
    username: str
    password: str


class HttpError(BaseException):
    detail: str


class AuthenticationException(Exception):
    pass


class GameIn(BaseModel):
    name: str
    description: Optional[str] = None
    platform: Optional[str] = None
    shelf_id: int
    account_id: int


class GameOut(BaseModel):
    id: int
    name: str
    description: Optional[str] = None
    platform: Optional[str] = None
    shelf_id: int
    account_id: int


class GameUpdate(BaseModel):
    name: str
    description: Optional[str] = None
    platform: Optional[str] = None


class ShelfIn(BaseModel):
    name: str
    account_id: int
    

class ShelfOut(BaseModel):
    id: int
    name: str
    account_id: int


class ShelfUpdate(BaseModel):
    name: str


class Shelf(BaseModel):
    id: int
    name: str
    account_id: Optional[int]
    games: List[GameOut] = []

    class Config:
        orm_mode = True
