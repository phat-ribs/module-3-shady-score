from fastapi import HTTPException, status
from psycopg_pool import ConnectionPool
from models import GameIn, GameOut, GameUpdate, HttpError
from typing import List
import os


pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class GamesQueries:
    def create_game(self, game_dict: GameIn) -> GameOut:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    try:
                        get_shelf_id = db.execute(
                            """
                            SELECT id FROM shelves
                            WHERE id = %s
                            """,
                            [game_dict["shelf_id"]]
                        )
                        shelf_id_slot = get_shelf_id.fetchone()
                        if shelf_id_slot is None:
                            raise HTTPException(
                                status_code=status.HTTP_404_NOT_FOUND,
                                detail="A shelf with the id you input does not exist in the database"
                            )

                        result = db.execute(
                            """
                            INSERT INTO games
                            (
                                name,
                                description,
                                platform,
                                shelf_id,
                                account_id
                            )   
                            VALUES
                            (%s, %s, %s, %s, %s)
                            RETURNING 
                                id,
                                name,
                                description,
                                platform,
                                shelf_id,
                                account_id
                            """,
                            [
                                game_dict["name"],
                                game_dict["description"],
                                game_dict["platform"],
                                game_dict["shelf_id"],
                                game_dict["account_id"]
                            ],
                        )
                        row = result.fetchone()
                        if row is not None:
                            record = {}
                            for i, column in enumerate(db.description):
                                record[column.name] = row[i]
                            return GameOut(**record)
                        else:
                            raise HttpError("Failed to fetch Game ID after creating")
                    except HttpError:
                        raise HTTPException(
                            status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Error creating a game"
                        )
                    

    def get_game(self, id: int, account_id: int, shelf_id: int) -> GameOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT *
                    FROM games
                    WHERE id = %s
                    AND account_id = %s
                    AND shelf_id = %s
                    AND account_id IS NOT NULL
                    AND shelf_id IS NOT NULL;
                    """,
                    [id, account_id, shelf_id],
                )
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                    return GameOut(**record)

                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Could not find a game with that Game ID, Account ID or Shelf ID"
                )


    def get_shelf_games(self, account_id: int, shelf_id: int) -> List[GameOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT *
                    FROM games
                    WHERE account_id = %s
                    AND shelf_id = %s
                    """,
                    [account_id, shelf_id],
                )
                rows = result.fetchall()
                games = []
                if rows:
                    for row in rows:
                        record = dict(zip([column.name for column in db.description], row))
                        games.append(GameOut(**record))
                    return games

                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Could not find games associated with the shelf matching the Account ID or Shelf ID given"
                )


    def get_user_games(self, account_id: int) -> List[GameOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT *
                    FROM games
                    WHERE account_id = %s;
                    """,
                    [account_id],
                )
                rows = result.fetchall()
                games = []
                if rows:
                    for row in rows:
                        record = dict(zip([column.name for column in db.description], row))
                        games.append(GameOut(**record))
                    return games

                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="No games have been added by this user..... yet"
                )
            

    def update_game(self, game_id: int, game_dict: GameUpdate, account_id: int, shelf_id: int) -> GameOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    db.execute(
                        """
                        UPDATE games
                        SET name = %s,
                            description = %s,
                            platform = %s
                        WHERE id = %s AND account_id = %s AND shelf_id = %s
                        """,
                        [
                            game_dict["name"],
                            game_dict["description"],
                            game_dict["platform"],
                            game_id,
                            account_id, 
                            shelf_id
                        ]
                    )
                except Exception as e:
                    raise Exception(f"Failed to update game. Error: {str(e)}")

                return GameOut(id=game_id, **game_dict)
            

    def delete_game(self, game_id: int, account_id: int) -> bool:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    id_check = db.execute(
                        """
                        SELECT * FROM games
                        WHERE id = %s
                        """,
                        [game_id]
                    )

                    id_row = id_check.fetchone()
                    if id_row is None:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="A game with that ID does not exist in the database"
                        )

                    account_id_check = db.execute(
                        """
                        DELETE FROM games
                        WHERE id = %s AND account_id = %s
                        """,
                        [
                            game_id,
                            account_id
                        ]
                    )
                    if account_id_check.rowcount == 0:
                        raise HTTPException(
                            status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="You are attempting to delete a game that you did not create"
                        )
                    return True
                