from fastapi import HTTPException, status
from psycopg_pool import ConnectionPool
from models import ShelfUpdate, ShelfIn, ShelfOut, GameOut, Shelf
from typing import List
import os


pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class ShelvesQueries:
    def create_shelf(self, shelf_dict: ShelfIn) -> ShelfOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                    result=db.execute(
                        """
                        INSERT INTO shelves (name, account_id) 
                        VALUES (%s, %s) 
                        RETURNING id, name, account_id
                        """,
                        (shelf_dict["name"], shelf_dict["account_id"]))
                    row = result.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                        return ShelfOut(**record)
                    

    def one_shelf_with_games(self, shelf_id: int, account_id: int) -> Shelf:
        with pool.connection() as conn:
            with conn.cursor() as db:

                db.execute("SELECT * FROM shelves WHERE id = %s", (shelf_id,))
                shelf_row = db.fetchone()
                if shelf_row is None:
                    raise HTTPException(status_code=404, detail="Shelf not found")

                db.execute("""
                SELECT id, name, description, platform, shelf_id, account_id
                FROM games
                WHERE account_id = %s AND shelf_id = %s 
                """,
                [account_id, shelf_id]
                )

                games = []
                for game_row in db.fetchall():
                    games.append(GameOut(
                        id=game_row[0],
                        name=game_row[1],
                        description=game_row[2],
                        platform=game_row[3],
                        shelf_id=game_row[4],
                        account_id=game_row[5]
                    ))

                return Shelf(id=shelf_row[0], name=shelf_row[1], account_id=shelf_row[2], games=games)


    def all_shelf_with_games(self, account_id: int) -> List[Shelf]:
        with pool.connection() as conn:
            with conn.cursor() as db:

                db.execute("SELECT * FROM shelves WHERE account_id = %s", (account_id,))
                shelf_rows = db.fetchall()
                print(shelf_rows)
                if not shelf_rows:
                    return []

                shelves = []
                for shelf_row in shelf_rows:
                    shelf_id = shelf_row[0]

                    db.execute("""
                    SELECT id, name, description, platform, shelf_id, account_id
                    FROM games
                    WHERE account_id = %s AND shelf_id = %s 
                    """,
                    [account_id, shelf_id]
                    )

                    games = []
                    for game_row in db.fetchall():
                        games.append(GameOut(
                            id=game_row[0],
                            name=game_row[1],
                            description=game_row[2],
                            platform=game_row[3],
                            shelf_id=game_row[4],
                            account_id=game_row[5],
                        ))
                    shelves.append(Shelf(
                        id=shelf_id,
                        name=shelf_row[1],
                        account_id=shelf_row[2],
                        games=games
                    ))

                return shelves


    def get_user_shelves(self, account_id: int) -> List[ShelfOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT * FROM shelves 
                    WHERE account_id = %s;
                    """,
                    [account_id])
                rows = db.fetchall()
                shelves = []
                if rows:
                    for row in rows:
                        record = dict(zip([column.name for column in db.description], row))
                        shelves.append(ShelfOut(**record))
                    return shelves
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Could not find any shelves associated with this account"
                )


    def get_user_shelf(self, shelf_id: int, account_id: int) -> ShelfOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT * FROM shelves 
                    WHERE id = %s AND account_id = %s
                    """,
                    [shelf_id, account_id])
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                    return ShelfOut(**record)
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Could not find any shelves associated with this account"
                )
            
        

    def update_shelf(self, shelf_id: int, shelf_dict: ShelfUpdate, account_id: int) -> ShelfOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    UPDATE shelves 
                    SET name = %s 
                    WHERE id = %s AND account_id = %s
                    """, 
                    [
                        shelf_dict["name"], 
                        shelf_id,
                        account_id
                    ])
                return ShelfOut(id=shelf_id, **shelf_dict)


    def delete_shelf(self, shelf_id: int, account_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM shelves 
                    WHERE id = %s 
                    AND account_id = %s
                    """, 
                    (shelf_id, account_id))
                if db.rowcount == 0:
                    raise HTTPException(status_code=404, detail="Shelf not found")
                conn.commit()
