from routers import accounts, shelves, games
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import MyAuthenticator


authenticator = MyAuthenticator(os.environ["SIGNING_KEY"])


app = FastAPI(prefix="/api")
app.include_router(authenticator.router, tags=['Auth'])
app.include_router(accounts.router, tags=['Auth'])
app.include_router(shelves.router, tags=['UserShelves'])
app.include_router(games.router, tags=['UserGames'])

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
